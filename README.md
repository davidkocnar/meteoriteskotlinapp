## Meteorites App

This Android app contains a list of meteorites, a filtering option and a detail screen with a map. On tablets it has a different (two pane) layout.

--------

### Assignment:

I was asked to make an application displaying meteorites sorted by size. The app should only display the ones from 2011 (I extended this task and added filtering by any date). The application should have offline (caching) functionality. When you click on a meteorite, you will see a map with a fall location. The UI should look good (I extended the UI with an alternative layout for tablets). Required minimum Android API was v16.

view: https://data.nasa.gov/view/ak9y-cwf9, 

documentation: https://dev.socrata.com/foundry/data.nasa.gov/y77d-th95

json: https://data.nasa.gov/resource/y77d-th95.json

administration: https://data.nasa.gov/login

--------

### Tech stack:

- Android SDK
- Kotlin (+ Kotlin Extensions)
- Android Architecture Components (Jetpack)
- RxJava 2
- Retrofit 2
- SimpleStatefulLayout
- Google Play Services - Maps API
- Timberkt (Timber Kotlin version)

--------

Minimal SDK (API) version: 16

Target SDK (API) version: 28
