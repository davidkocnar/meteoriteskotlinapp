package cz.davidkocnar.meteoritetestapp.ui

import android.os.Bundle
import android.support.annotation.IdRes
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import cz.davidkocnar.meteoritetestapp.R
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import cz.davidkocnar.meteoritetestapp.ui.MeteoriteDetailActivity.Companion.ARG_METEORITE_OBJ

/**
 * A fragment representing a single Meteorite detail screen.
 * This fragment is either contained in a [MeteoriteListActivity]
 * in two-pane mode (on tablets) or a [MeteoriteDetailActivity]
 * on handsets.
 */
class MeteoriteDetailFragment : Fragment(), OnMapReadyCallback {
    private var meteorite: MeteoriteResponse? = null
    private lateinit var rootView: View
    private lateinit var mapFragment: SupportMapFragment

    companion object {
        fun loadFragment(@IdRes res: Int, item: MeteoriteResponse, supportFragmentManager: FragmentManager) {
            val arguments = Bundle()
            arguments.putParcelable(ARG_METEORITE_OBJ, item)
            val fragment = MeteoriteDetailFragment()
            fragment.arguments = arguments
            supportFragmentManager.beginTransaction()
                    .replace(res, fragment)
                    .commit()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (arguments != null && arguments!!.containsKey(ARG_METEORITE_OBJ)) {
            meteorite = arguments!!.getParcelable(ARG_METEORITE_OBJ) as MeteoriteResponse
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.meteorite_detail, container, false)
        mapFragment = (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?)!!
        mapFragment.getMapAsync(this)
        return rootView
    }

    override fun onResume() {
        mapFragment.onResume()
        super.onResume()
    }

    override fun onPause() {
        mapFragment.onPause()
        super.onPause()
    }

    override fun onLowMemory() {
        mapFragment.onLowMemory()
        super.onLowMemory()
    }

    override fun onDestroy() {
        mapFragment.onDestroy()
        super.onDestroy()
    }

    override fun onMapReady(googleMap: GoogleMap) = if (meteorite?.geolocation?.getLat() != null && meteorite?.geolocation?.getLng() != null) {
        val geo = meteorite!!.geolocation!!

        val position = LatLng(geo.getLat(), geo.getLng())
        val markerOptions = MarkerOptions()
                .position(position)
                .title(meteorite!!.name)
                .snippet(meteorite!!.getMassPrepared(rootView.context.applicationContext))
                .visible(true)

        val cameraUpdate = CameraUpdateFactory.newLatLngZoom(position, 5f)
        googleMap.addMarker(markerOptions).showInfoWindow()
        googleMap.moveCamera(cameraUpdate)
    } else {
        Snackbar.make(rootView, getString(R.string.snack_geolocation_unknown), Snackbar.LENGTH_LONG).show()
    }
}
