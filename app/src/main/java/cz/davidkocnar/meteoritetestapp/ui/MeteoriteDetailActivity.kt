package cz.davidkocnar.meteoritetestapp.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import cz.davidkocnar.meteoritetestapp.R
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import kotlinx.android.synthetic.main.activity_meteorite_detail.*


/**
 * An activity containing [MeteoriteDetailFragment].
 * This activity is used only for handsets (phones).
 */
class MeteoriteDetailActivity : AppCompatActivity() {

    companion object {
        const val ARG_METEORITE_OBJ = "ARG_METEORITE_OBJ"
        fun startActivity(context: Context, item: MeteoriteResponse) {
            val intent = Intent(context, MeteoriteDetailActivity::class.java)
            intent.putExtra(ARG_METEORITE_OBJ, item)
            context.startActivity(intent)
        }
    }

    private lateinit var item: MeteoriteResponse

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meteorite_detail)
        setSupportActionBar(detail_toolbar)

        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        title = " "

        item = intent.getParcelableExtra(ARG_METEORITE_OBJ) as MeteoriteResponse

        if (savedInstanceState == null) {
            MeteoriteDetailFragment.loadFragment(R.id.meteorite_detail_container, item, supportFragmentManager)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}
