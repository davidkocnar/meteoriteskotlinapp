package cz.davidkocnar.meteoritetestapp.ui

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import cz.davidkocnar.meteoritetestapp.R
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import cz.davidkocnar.meteoritetestapp.viewmodel.MeteoritesViewModel
import kotlinx.android.synthetic.main.meteorite_list_content.view.*


/**
 * Adapter with clickable meteorites items
 */
internal class MeteoritesRecyclerViewAdapter constructor(
        viewModel: MeteoritesViewModel,
        private var listData: List<MeteoriteResponse>?) : RecyclerView.Adapter<MeteoritesRecyclerViewAdapter.ViewHolder>() {

    private val onClickListeners: (MeteoriteResponse) -> Unit = viewModel.onItemClickListener
    private var layoutInflater: LayoutInflater? = null

    override fun getItemCount(): Int = listData?.size ?: 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        layoutInflater = layoutInflater ?: LayoutInflater.from(parent.context)
        val view = layoutInflater!!.inflate(R.layout.meteorite_list_content, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val meteorite = listData?.get(position)
        meteorite?.let {
            holder.bind(meteorite, onClickListeners)
        }
    }

    internal inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val tvMeteoriteName = view.id_text
        private val tvMass = view.mass
        private val tvDate = view.date

        fun bind(meteorite: MeteoriteResponse, clickListener: (MeteoriteResponse) -> Unit) {
            tvMeteoriteName.text = meteorite.name
            tvDate.text = meteorite.getYearPrepared(itemView.context.applicationContext)
            tvMass.text = meteorite.getMassPrepared(itemView.context.applicationContext)
            itemView.setOnClickListener{ clickListener(meteorite) }
            itemView.tag = meteorite
        }
    }
}
