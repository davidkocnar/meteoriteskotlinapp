package cz.davidkocnar.meteoritetestapp.ui

import android.app.DatePickerDialog
import android.arch.lifecycle.Observer
import android.databinding.Observable
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.design.widget.CollapsingToolbarLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.ViewGroup
import com.blankj.utilcode.util.Utils
import com.github.ajalt.timberkt.Timber
import com.google.android.gms.maps.MapsInitializer
import cz.davidkocnar.meteoritetestapp.BuildConfig
import cz.davidkocnar.meteoritetestapp.R
import cz.davidkocnar.meteoritetestapp.data.MeteoritesRepository
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import cz.davidkocnar.meteoritetestapp.viewmodel.CalendarEvent
import cz.davidkocnar.meteoritetestapp.viewmodel.MeteoritesViewModel
import cz.davidkocnar.meteoritetestapp.viewmodel.OpenDetailEvent
import kotlinx.android.synthetic.main.activity_meteorite_list.*
import kotlinx.android.synthetic.main.meteorite_list_layout.view.*



/**
 * An activity containing list of meteorites and [CollapsingToolbarLayout].
 * This activity contains also [MeteoriteDetailFragment] in two-pane mode (on tablets)
 * or only [RecyclerView] on handsets.
 */
class MeteoriteListActivity : AppCompatActivity() {

    private val viewModel by lazy { MeteoritesViewModel.create(this, MeteoritesRepository(applicationContext)) }
    private val onStateChanged = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable, propertyId: Int) {
            stateful_layout.state = viewModel.state.get()
        }
    }
    private var adapter: MeteoritesRecyclerViewAdapter? = null
    private var meteoritesCount = 0
    private var scrolled = false
    private var twoPaneLayout: Boolean = false  // if two pane layout (tablet) is active

    private val statusBarHeight: Int
        get() {
            var result = 0
            val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
            if (resourceId > 0) {
                result = resources.getDimensionPixelSize(resourceId)
            }
            return result
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_meteorite_list)
        toolbarSetup()
        MapsInitializer.initialize(this)

        meteorite_detail_container?.let {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPaneLayout = true
        }

        if (BuildConfig.DEBUG) {
            Timber.uprootAll()
            Timber.plant(Timber.DebugTree())
        }

        viewModelSetup()
    }

    private fun toolbarSetup() {
        setSupportActionBar(meteorites_layout.toolbar)
        meteorites_layout.toolbar.title = ""
        meteorites_layout.toolbar.titleMarginTop = statusBarHeight
        val tv = TypedValue()
        var actionBarHeight = 0
        if (theme.resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, resources.displayMetrics)
        }
        meteorites_layout.toolbar.layoutParams = CollapsingToolbarLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                actionBarHeight + statusBarHeight)
        if (supportActionBar != null) supportActionBar?.setDisplayShowTitleEnabled(false)

        meteorites_layout.app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            val totalScroll = appBarLayout.totalScrollRange
            val currentScroll = totalScroll + verticalOffset

            if (currentScroll == 0) {
                if (!scrolled) {
                    scrolled = true
                    refreshActivityTitle()
                }
            } else {
                if (scrolled) {
                    scrolled = false
                    refreshActivityTitle()
                }
            }
        })
    }

    private fun viewModelSetup() {
        Utils.init(this)
        viewModel.state.addOnPropertyChangedCallback(onStateChanged)
        viewModel.meteorites.observe(this, Observer<List<MeteoriteResponse>> { this.setAdapterData(it) })
        viewModel.openDatePickerDialogEvent.observe(this, Observer<CalendarEvent> { event -> this.openDatepickerDialog(event!!) })
        viewModel.openMeteoriteDetailEvent.observe(this, Observer<OpenDetailEvent>(this::openMeteoriteDetail))
        viewModel.initViewModelData()
        stateful_layout.setOfflineRetryOnClickListener(viewModel.offlineRetryListener)
        meteorites_layout.filter_option.setOnClickListener{viewModel.filterBtnClicked()}
    }

    private fun refreshActivityTitle() {
        if (scrolled) {
            meteorites_layout.toolbar_layout.title = getString(R.string.title_x_meteorites, meteoritesCount)
        } else {
            meteorites_layout.toolbar_layout.title = meteoritesCount.toString()
        }
        meteorites_layout.meteorites_subtitle.text = getString(R.string.meteorites_subtitle, viewModel.lastDateString)
    }

    private fun setAdapterData(meteorites: List<MeteoriteResponse>?) {
        adapter = MeteoritesRecyclerViewAdapter(
                this.viewModel,
                meteorites)
        runOnUiThread { adapter?.notifyDataSetChanged() }
        meteorites_layout.recycler_view.adapter = adapter
        if (meteorites != null) {
            meteoritesCount = meteorites.size
            refreshActivityTitle()
        }
    }

    private fun openDatepickerDialog(event: CalendarEvent) {
        if (event.isNotHandled()) {
            val datePickerDialog = DatePickerDialog(this, viewModel,
                    event.year, event.month, event.day)
            datePickerDialog.show()
        }
    }

    private fun openMeteoriteDetail(event: OpenDetailEvent?) {
        if (event != null && event.isNotHandled()) {
            if (twoPaneLayout) {
                MeteoriteDetailFragment.loadFragment(
                        R.id.meteorite_detail_container,
                        event.meteorite,
                        supportFragmentManager)
            } else {
                MeteoriteDetailActivity.startActivity(
                        this,
                        event.meteorite)
            }
        }
    }
}
