package cz.davidkocnar.meteoritetestapp.data.model

import android.content.Context
import android.databinding.BaseObservable
import android.os.Parcelable
import com.github.ajalt.timberkt.Timber
import cz.davidkocnar.meteoritetestapp.R
import kotlinx.android.parcel.Parcelize
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class MeteoriteResponse(
        var fall: String? = null,
        var geolocation: Geolocation? = null,
        var id: Int,
        var mass: Double,
        var name: String,
        var nametype: String? = null,
        var recclass: String? = null,
        var reclat: Double? = null,
        var reclong: Double? = null,
        var year: String
) : BaseObservable(), Parcelable {

    fun getMassPrepared(context: Context): String {
        return if (this.mass >= 10000) {
            val massKg = this.mass / 1000
            val resultMass = String.format(Locale.ENGLISH, "%,.0f", massKg).replace(",", " ")
            context.resources.getString(R.string.mass_string_kg, resultMass)
        } else {
            val resultMass = String.format(Locale.ENGLISH, "%,.0f", this.mass).replace(",", " ")
            context.resources.getString(R.string.mass_string_g, resultMass)
        }
    }

    fun getYearPrepared(context: Context): String {
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH)
        return try {
            val yourDate = parser.parse(year)
            val calendar = Calendar.getInstance()
            calendar.time = yourDate
            context.resources.getString(R.string.year_string, calendar.get(Calendar.YEAR))
        } catch (e: ParseException) {
            Timber.w{"Error while parsing year string! year=$year"}
            ""
        }
    }
}
