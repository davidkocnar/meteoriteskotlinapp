package cz.davidkocnar.meteoritetestapp.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Geolocation(
        var type: String? = null,
        var coordinates: List<Double>  // Type List because of source data structure
) : Parcelable {

    fun getLng() = coordinates[0]
    fun getLat() = coordinates[1]
}
