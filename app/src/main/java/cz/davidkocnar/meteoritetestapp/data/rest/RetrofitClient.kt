package cz.davidkocnar.meteoritetestapp.data.rest

import android.content.Context
import com.blankj.utilcode.util.NetworkUtils
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.CacheControl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File


object RetrofitClient {

    private const val BASE_URL = "https://data.nasa.gov/resource/"
    private const val ONE_DAY = 86400  // 60*60*24 s = 1 day
    private const val CACHE_SIZE: Long = 5242880  // 5 * 1024 * 1024 = 10 MB

    private val gson = GsonBuilder()
            .setLenient()
            .create()
    private lateinit var okHttpClient: OkHttpClient
    private var retrofit: Retrofit? = null

    fun getClient(context: Context): Retrofit {
        if (this.retrofit == null) {
            okHttpClient = OkHttpClient.Builder()
                    .cache(Cache(File(context.cacheDir, "apiResponses"), CACHE_SIZE))
                    .addInterceptor { chain ->
                        var request = chain.request()
                        request = if (NetworkUtils.isConnected()) {  // When network is active
                            request.newBuilder().header("Cache-Control", "public, max-age=$ONE_DAY").build()
                        } else {  // When network is inactive, offline mode
                            request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build()
                        }
                        chain.proceed(request)
                    }
                    .build()

            retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build()
        }
        return this.retrofit!!
    }
}
