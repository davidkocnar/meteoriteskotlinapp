package cz.davidkocnar.meteoritetestapp.data.rest

import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by David on 20.06.2018.
 * Description:
 */
interface ApiInterface {

    @GET("y77d-th95.json")
    fun getMeteorites(@Query("\$order") order: String,
                      @Query("\$where") where: String,
                      @Query("\$limit") limit: Int,
                      @Query("\$offset") offset: Int,
                      @Query("$\$app_token") token: String): Observable<List<MeteoriteResponse>>
}
