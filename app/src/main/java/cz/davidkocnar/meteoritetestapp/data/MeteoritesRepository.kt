package cz.davidkocnar.meteoritetestapp.data

import android.content.Context
import cz.davidkocnar.meteoritetestapp.BuildConfig
import cz.davidkocnar.meteoritetestapp.R
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import cz.davidkocnar.meteoritetestapp.data.rest.ApiInterface
import cz.davidkocnar.meteoritetestapp.data.rest.RetrofitClient
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*

/**
 * Repository class requesting remote data.
 */
class MeteoritesRepository(context: Context) {

    companion object {
        private const val LIMIT = 100000  // default LIMIT for results - simple use case
        private const val ORDER = "mass DESC"
        private const val DATE_PATTERN = "yyyy-MM-dd"
        private const val TOKEN = BuildConfig.NASA_TOKEN  // NASA data token
        val format = SimpleDateFormat(DATE_PATTERN, Locale.ENGLISH)
    }

    private val appContext: Context = context.applicationContext
    private val apiInterface by lazy { RetrofitClient.getClient(context).create(ApiInterface::class.java) }

    fun requestData(offset: Int, date: Date): Observable<List<MeteoriteResponse>> {
        val formattedDate = format.format(date)
        val where = appContext.getString(R.string.rest_param_where, formattedDate)
        val call = apiInterface.getMeteorites(ORDER, where, LIMIT, offset, TOKEN)

        return call.subscribeOn(Schedulers.io())
    }
}
