package cz.davidkocnar.meteoritetestapp.viewmodel

import android.app.DatePickerDialog
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.ObservableField
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.DatePicker
import com.github.ajalt.timberkt.Timber
import cz.davidkocnar.meteoritetestapp.data.MeteoritesRepository
import cz.davidkocnar.meteoritetestapp.data.MeteoritesRepository.Companion.format
import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse
import cz.kinst.jakub.view.SimpleStatefulLayout
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.text.ParseException
import java.util.*

/**
 * ViewModel class using [MeteoritesRepository] to retrieve remote data.
 * It has MutableLiveData fields ready for observers.
 * [Context] is only Application Context (not Activity).
 */
class MeteoritesViewModel(private val repository: MeteoritesRepository) : ViewModel(), DatePickerDialog.OnDateSetListener {

    val meteorites = MutableLiveData<List<MeteoriteResponse>>()
    val openDatePickerDialogEvent = MutableLiveData<CalendarEvent>()
    val openMeteoriteDetailEvent = MutableLiveData<OpenDetailEvent>()
    var state = ObservableField<String>()
    val lastDateString: String
        get() = format.format(dateForFiltering.time)

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val dateForFiltering = Calendar.getInstance()

    val onItemClickListener: (MeteoriteResponse) -> Unit = { meteorite ->
        Timber.d {"Meteorite Item clicked and handled by ViewModel: $meteorite"}
        openMeteoriteDetailEvent.value = OpenDetailEvent(meteorite)
    }
    val offlineRetryListener = View.OnClickListener { initViewModelData() }

    /*
     * Set default date when the ViewModel is created
     */
    init {
        try {
            dateForFiltering.time = format.parse(DATE_DEFAULT)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
    }

    /*
     * Initialize new data from repository
     * Offset is always zero for this task (all data should be loaded at once)
     */
    fun initViewModelData() {
        state.set(SimpleStatefulLayout.State.PROGRESS)
        if (this.meteorites.value == null || this.meteorites.value!!.isEmpty()) {
            meteorites.postValue(null)
            compositeDisposable.add(this.repository.requestData(0, dateForFiltering.time)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::receiveNextData, this::setRequestError))

        } else if (this.meteorites.value != null) {
            useLoadedData()
        }
    }

    private fun receiveNextData(response: List<MeteoriteResponse>) {
        Timber.d { "Data received. size:${response.size}" }
        if (response.isEmpty()) {
            setEmpty()
        } else {
            setData(response)
        }
    }

    private fun setData(data: List<MeteoriteResponse>) {
        meteorites.postValue(data)
        state.set(SimpleStatefulLayout.State.CONTENT)
    }

    private fun setEmpty() {
        state.set(SimpleStatefulLayout.State.EMPTY)
    }

    private fun setRequestError(throwable: Throwable) {
        Timber.e { "Request error! $throwable" }
        state.set(SimpleStatefulLayout.State.OFFLINE)
    }

    private fun useLoadedData() {
        state.apply {
            set(SimpleStatefulLayout.State.CONTENT)
            notifyChange()
        }
    }

    /*
     * Activity filter button was clicked
     */
    fun filterBtnClicked() {
        Timber.d { "Filter btn clicked!" }
        openDatePickerDialogEvent.value = CalendarEvent(dateForFiltering)
    }

    /*
     * Datepicker callback - on dialog closed
     */
    override fun onDateSet(datePicker: DatePicker, year: Int, month: Int, day: Int) {
        dateForFiltering.set(year, month, day)
        meteorites.value = null
        initViewModelData()
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    companion object {
        fun create(activity: AppCompatActivity, repository: MeteoritesRepository): MeteoritesViewModel {
            return ViewModelProviders
                    .of(activity, MeteoritesViewModel.Factory(repository))
                    .get(MeteoritesViewModel::class.java)
        }

        const val DATE_DEFAULT = "2011-01-01"
    }

    /*
	 * Factory class of ViewModel
	 */
    class Factory
    constructor(private val repository: MeteoritesRepository) : ViewModelProvider.Factory {

        @Suppress("UNCHECKED_CAST")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            return MeteoritesViewModel(repository) as T
        }
    }

}

