package cz.davidkocnar.meteoritetestapp.viewmodel

import cz.davidkocnar.meteoritetestapp.data.model.MeteoriteResponse

/**
 * Event class with inner data [MeteoriteResponse]
 */
class OpenDetailEvent(val meteorite: MeteoriteResponse) {
    var hasBeenHandled = false

    fun isNotHandled(): Boolean {
        return if (hasBeenHandled) {
            false
        } else {
            hasBeenHandled = true
            true
        }
    }
}

