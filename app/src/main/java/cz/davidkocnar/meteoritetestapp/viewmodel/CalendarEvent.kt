package cz.davidkocnar.meteoritetestapp.viewmodel

import java.util.*

/**
 * Event class with inner data [Calendar]
 */
class CalendarEvent(private val calendar: Calendar) {
    private var hasBeenHandled = false

    fun isNotHandled(): Boolean {
        return if (hasBeenHandled) {
            false
        } else {
            hasBeenHandled = true
            true
        }
    }

    val year: Int
        get() = calendar.get(Calendar.YEAR)

    val month: Int
        get() = calendar.get(Calendar.MONTH)

    val day: Int
        get() = calendar.get(Calendar.DAY_OF_MONTH)
}
